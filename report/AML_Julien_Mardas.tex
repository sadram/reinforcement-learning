%  texi2pdf --clean -s AML_Article_JMardas.tex 
\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[toc,page]{appendix}
\usepackage[nottoc]{tocbibind}
\usepackage[T1]{fontenc}
\usepackage[]{csquotes} 
\usepackage[]{hyperref}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{listings}             
\usepackage{enumitem}
\usepackage{caption}
\usepackage{array}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage[]{caption}
\usepackage[]{subcaption}
\usepackage[]{placeins}
\usepackage{listings}
\usepackage{color}
\usepackage{algpseudocode}
\usepackage{algorithm}
\usepackage{float}


% for code source inclusion in Python
\usepackage{listings}
\usepackage{color}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\definecolor{green}{rgb}{0,0.6,0}
\definecolor{amber}{rgb}{1.0, 0.49, 0.0}
\definecolor{cadmiumgreen}{rgb}{0.0, 0.42, 0.24}
\definecolor{arsenic}{rgb}{0.23, 0.27, 0.29}

\lstset{
  frame=tb,
  language=Java,
  captionpos=b, 
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  breakatwhitespace=true,
  tabsize=4,
  title=\lstname,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  rulecolor=\color{black},
  %numbers=none,
  numbers=left,
  numbersep=5pt, 
  numberstyle=\tiny\color{gray},
  keywordstyle=\bfseries\color{blue},
  commentstyle=\color{cadmiumgreen},
  identifierstyle=\color{arsenic},
  stringstyle=\color{amber}
}
\setlength\parindent{0pt}
\setlist{noitemsep}
\lstset{language=Python} 

\title{Advanced Machine Learning Project \\ - \\ Ecole Centrale Paris } 
\author{Julien Mardas}
\date{\today}

\hypersetup{
    %bookmarks=true,         % show bookmarks bar?
    unicode=false,           % non-Latin characters 
    pdftoolbar=true,        
    pdfmenubar=true,        
    pdffitwindow=true,     
    pdfstartview={FitH},    
    pdftitle={Q-learning strategy in the Wumpus game},    % titlex
    pdfauthor={Julien Mardas},     % author
    pdfsubject={Q-learning strategy in the Wumpus game},   
    pdfkeywords={Q-learning, sarsa, Softmax, reinforcement, learning},
    pdfnewwindow=true,      % links in new PDF window
    % pdfcreator={Creator},   
    % dfproducer={Producer},
    %colorlinks=true,       % false: boxed links; true: colored links
    %linkcolor=red,          % color of internal links (change box color with linkbordercolor)
    %citecolor=green,        % color of links to bibliography
    %filecolor=magenta,      % color of file links
    %urlcolor=cyan           % color of external links
}



\begin{document}
\maketitle
\vspace*{4\baselineskip}

\begin{abstract}
\bigskip
    In this article we will study the Q-learning reinforcement strategy using the Wumpus game. We will  try to find a good compromise between long term reward exploitation and exploration to be as close as possible to an optimal policy in a deterministic and stochastic environment.
    
    The source code of the project and this article can be found at: 
    \vspace{1\baselineskip}
    
    \url{https://bitbucket.org/sadram/aml_reinforcement_learning}.
\end{abstract}

\newpage
\tableofcontents
\newpage

\section{Introduction}
\bigskip
The Wumpus game is a toy program that we will use to help us compare artificial intelligence algorithms' efficiency to accomplish and learn a certain task that is to maximize the reward of the game. 
\par\medskip
Our work will strive to study reinforcement learning algorithms and benchmark them to asses there relative efficiencies. The problem is a Markov Decision Process (MDP) with Temporal Differences (TD) each time slice being a step or move during the game.
\par\medskip
The future state depends on the current state and the problem is fully described by the probability of transitioning between two states with respect to the initial conditions. Each state can be evaluated and ranked using a value function.
However and as usual, we don't have prior information on those parameters. In this paper we will focus on using the Q-learning algorithm to solve this problem.
\par\medskip
Q-learning is a dynamic programming algorithm used in reinforcement learning to maximize a reward on the long term over several experiments. Q-learning uses on-line learning to update the value of a state knowing past experiments (game history). This algorithm can converge to an optimal policy without prior information on the value of a state or the probability to transition between the two states. We can hence model the game as a MDP and relax the missing assumptions with Q-learning.
\par\medskip
Q-learning's convergence speed depends on the fine tuning of its parameters. They could be fixed, adaptive or dependent on other variables of the problem. 
We will try to find a setup that yields a good compromise between long term reward exploitation and exploration. Tests will be conducted in a deterministic and in a stochastic environment.
\newpage

\section{Design and methodology}
\bigskip

\subsection{The game rules}
\bigskip

In the Wumpus game, the Agent (Einstein) must find a treasure (Scientific paper) while avoiding all the traps - being caught by the Wumpus (Octopus) and falling in the black-hole. 
There are two possible actions: move or flash. Only one action at a time can be performed in four directions (up, down, left, right) by 1 step (neighbor boxes on the board). 
\par\medskip

If the Wumpus and the agent meet, or if the agent steps on the black-hole, the Agent dies. If the Agent flashes to the box were the Wumpus is, the Wumpus is killed.
The game ends when the Agent dies or when he finds the treasure. Rewards are computed with respect to the Table~\ref{reward_table}.
\bigskip

\begin{table}[!ht]
    \centering
    \begin{tabular}{@{}ll@{}}
        \toprule
        Event                     & Reward \\ \midrule
        Finding the treasure      & +100   \\
        killing the Wumpus        & +10    \\
        falling in the black-hole & -10    \\
        encounter the Wumpus      & -10    \\
        default                   & -1     \\ \bottomrule
    \end{tabular}
    \caption{fixed reward per event}
    \label{reward_table}
\end{table}\bigskip

A negative reward is used for the default action to avoid infinite loops that can maximize the local reward but won't solve the game. It will also force the agent to find more rewarding events.
\par\medskip

We use the term "a game" to indicate a number of consecutive episodes (given by the -e | --episodes option) with the same setup played by the Agent (the artificial intelligence) using a learning policy.
\par\medskip

A game setup is coded with the following string:

\begin{gather*}
E_nP_nF_nX_nT_bW_{nb}H_n[POL1|2]\ (n \in \mathbb{N},\ b \in \mathbb{B})
\end{gather*}

This code means:

\begin{itemize}
\renewcommand\labelitemi{--}
    \item $E_n$ is the number of episodes played (default=100, max=10000)
    \item $P_n$ is the policy identified by a number (eg.: P5 is the custom policy, $n\in [[1, 7]]$)
    \item $F_n$ is the number of flashes available at the beginning (default=2, max=100)
    \item $X_n$ is the grid size ($n \times n$, default = $4 \times 4$, max=$20 \times 20$)
    \item $T_b$ is a Boolean defining the board as a tore 
    \item $W_{nb}$ where $n$ is an integer defining the number of Wumpuses (default 1) and $b$ is a Boolean defining whether the Wumpuses are dynamic (moving) or not (default is False)
    \item $H_n$ is the number of black holes
    \item The integer representation for the policies are Random(1), Greedy(2), e-Greedy(3), Softmax(4), UCB(5), Custom(6), and Q-learning(7)
    \item while using the Q-learning strategy, [POL1|2] will define the associated policy which 1 for Softmax and 2 for $\epsilon-$greedy.
\end{itemize}
\bigskip

\textit{e.g.\ }E100P5F2X4T1W1 means Agent with UCB Policy, 2 Flashes, $ 4 \times 4 $ Tore Grid with a moving Wumpus for 100 games. 
\par\bigskip
For a detailed help on the program usage and its options simply type the following in a shell prompt:
\begin{verbatim}
$ Wumpus --help
\end{verbatim}
\bigskip

\subsubsection{New changes}
\bigskip

Changes were made to allow having more than one Wumpus or more than one black-hole. 
\par\medskip

The Wumpuses can move freely. The only policy for a Wumpus movement is the random policy. 
When two Wumpuses cross roads as when the Wumpus walks on a black hole nothing happens.
\par\medskip

Those setups could be changed to enhance the game and find new strategies as for instance trying to push the Wumpus in a black-hole or trying to kill two Wumpuses with one flash. Hence, the game can be totally non-deterministic.
\bigskip

\subsection{The Q-learning strategy}
\bigskip

\subsubsection{The model} 
\bigskip

The $(\epsilon-)$greedy, custom and Softmax policies gave very good results after a big number of trials. The Upper Confidence Bound strategy proved to be very helpful to minimize the regret and choose the best possible action. 
\par\medskip

However, each step of the game is independent (multi-armed bandit) and doesn't take into account the long term result of a chain of moves thus providing slow and unsatisfactory outcomes in non-deterministic or dynamic environments.
\par\medskip

It could be great to adapt the value of an action to the long term outcome and consequently approximate an optimal policy by learning at each state the impact of an action. This is what Q-learning is about.
\par\medskip
Let's define our value function as the sum of the current state reward plus the value of the next one:

\begin{gather}
V(s_t) = R(s_t) + \gamma\ V(s_{t+1})\ with\ 
    \begin{cases}
     s_t & \text{ state at time}\ t \\ 
     V(s_t) & \text{ value of the state}\ s_t\\ 
     R_t & \text{ instant reward at time}\ t \\
     \gamma \in [0,1] & \text{ discount factor} \\
    \end{cases}
\end{gather}
\par\medskip

As we are in a timed game -- each movement can be seen as a slice of time $\Delta t$. Here $t\in \mathbb{N}$. Hence, the function value can be defined as function that depends on the current state and the action taken at time $t$. So we can use a Temporal Difference (TD) learning algorithm to approximate $V(s_t, a_t)$ and write:
\begin{gather}
V(s_t, a_t) = R(s_t) + \alpha\ [R_{t+1} + \gamma\ V(s_{t+1}, a_t) ]\ with \
    \begin{cases}
     a_t & \text{ action at time}\ t \\ 
     V(s_t, a_t) & \text{ value of state}\ s_t\ \text{after}\ a_t\\ 
     R(s_t) & \text{ instant reward of at time}\ t \\
     \alpha \in [0,1] & \text{ learning rate} \\
    \end{cases}
\end{gather}

The idea here is to approximate the value of a state -- with respect to an action and a policy -- by the instant reward of the current state plus the discounted value of the next state -- we reached with action $a_t$. The discount factor $\gamma$ that tells us how important is the next state to be considered. With higher values it encourages the agent to seekout reward sooner than later. This factor could be time dependant or static. The $\alpha$ factor is the learning rate, with highers values it gives more importance to learning new values. As for gamma this factor could also be time dependent.
\par\medskip
In a determinist environment the value of $V(s_t)$ can be written as the average of all the values that leads to $s_t$ with respect to the probability distribution of transitioning from any state to this state:

\begin{gather*}
V(s_t) =\mathbb{E}[\ \sum_{s'\in S}P(s'|s_t, r=r_t, a=a_t) . V(s')\ ]
\end{gather*}

Since we have no prior knowledge on the distribution, the idea of the Q-learning is to use active learning to approximate this value and maximise it with respect to a policy $\pi$:

\begin{align}
& Q^\pi(s_t, a_t) =\mathbb{E}[\ \sum_{s'\in S}P(s'|s_t, r=r_t, a=a_t) . V^\pi(s')\  && \text{with, } \nonumber \\
& V^\pi(s_t) =  \max_{a \in A} Q^\pi(s_t, a) && \text{hence, } \nonumber  \\
& Q^\pi(s_t,a_t) = R(s_t) + \gamma . \mathbb{E}[\ \sum_{s'\in S}P(s'|s_t, r=r_t, a=a_t).\max_{a' \in A} Q^\pi(s_t, a')\ ]
\end{align}

Now we can rewrite (2) and get an expression of the long term reward using the Q-learning algorithm:

\begin{gather}
Q^\pi(s_t, a_t) = Q^\pi(s_t, a_t) + \alpha [r_{t+1} + \gamma \max_{a \in A} Q^\pi(s_{t+1}, a) - Q^\pi(s_t, a_t) ]
\end{gather}
\bigskip

\subsubsection{Explanation}
\bigskip

If the discount factor $\gamma = 0$ the outcome is equivalent to a greedy strategy if $\gamma = 1$ we focus on seeking a better reward at long term. We must be aware In our setup the default will $\gamma = 0.99$.
\par\medskip
If the learning rate $\alpha = 0$ we are only using the instant reward which makes the agent blind to the consequences of the next actions. On the other hand if $\alpha = 1$ we will favor the future values as much as our prior knowledge. To be neutral, we will default to $\alpha = 0.5$.
\par\medskip 
This approach solves the problem and is proved to converge \cite{watkins} to an optimal policy.
\par\medskip 

Yielding the long term rewards off all the possible actions that leads to a state and for all possible state can be very costly. Since the Wumpus game has a finite space state and finite set of actions it doesn't impact this study.
\par\medskip 

As is, this algorithm has theoretically no termination. So we could either bound it to a finite number of iterations or, break the loop when the long term reward $Q^\pi(s,a)$ seems fairly stable for each couple of actions and states.
\par\medskip

In the case of a stochastic environment the learning can be long. One way of tuning it could be to have adaptive values for $\alpha and \gamma$ at each state of the game (using evolutionary strategy could be a clue).
\par\medskip

Finally, there could be cases were some state would be either catastrophic or very high valued regardless of the next moves (eg. connect four game with three coins of the same color without no enemy coins on the surroundings, four corners taken by a player in reversi, four aces at poker, ...). We don't need to explore all those cases more than once. To solve this case we could introduce a loss function that will maximize (resp. penalize) values with a corresponding seen pattern.
\bigskip

\subsubsection{Implementation}
\bigskip

We implemented the Q-learning strategy with two policies:
\begin{itemize}
\renewcommand\labelitemi{--}
\item $\epsilon-$Greedy policy with $\epsilon=10\%$ to keep an exploratory behaviour 
\item Softmax policy with a a temperature parameter of 1 as we want to maximize the highest expected reward
\end{itemize}
\bigskip

The equation (4) is a Bellman equation modelling our Markovian Decision Process. Our implementation will approximate it using numerical backward induction.
\par\medskip
The implementation was done in Python 3. The detailed code of the Q-learning agent can by found in the listing \ref{qlearningagent}.
\par\medskip


\lstset{caption={Q-learning agent implementation in Python},label=qlearningagent}
\begin{lstlisting}
class QLearningPolicy(Reward):
    def __init__(self, epsilon=0.1, tau=1.0, gamma=0.99, alpha=0.5, softmax=0):
        super().__init__()
        self.Q_ = dict()
        self.alpha_ = alpha
        self.epsilon_ = epsilon
        self.gamma_ = gamma
        self.tau_ = tau
        self.softmax = softmax

    def getAction(self):
        return super().getAction()

    def getNextAction(self):
        # Softmax policy
        if self.softmax == 1:
            if self.Q_:
                softmax_proba = [np.exp(e) / self.tau_ for e in self.Q_[str(self.state_vector_)].values()]
                total_reward = sum(softmax_proba)
                softmax_proba = [p / total_reward for p in softmax_proba]
                return Action(np.argmax(np.random.multinomial(1, softmax_proba)) + 1)
            else:
                return self.getActionRandom()
        # e-greedy
        if self.softmax == 0:
            draw = np.random.uniform()
            if draw > self.epsilon_:
                return max(self.Q_[str(self.state_vector_)], key=self.Q_[str(self.state_vector_)].get)
            else:
                return self.getActionRandom()

    def nextState(self, s, reward):
        if self.init:
            oldstate = str(self.state_vector_)
            if oldstate not in self.Q_.keys():
                self.Q_[oldstate] = {act: 0 for act in Action}
            qold = self.Q_[oldstate][self.prev_action_]

        super().nextState(s, reward)
        if self.prev_action_ == -1:
            return

        n_state = str(self.state_vector_)
        if n_state not in self.Q_.keys():
            self.Q_[n_state] = {act: 0 for act in Action}

        qmax = max(self.Q_[n_state], key=self.Q_[n_state].get)
        deltav = reward + self.gamma_ * self.Q_[n_state][qmax] - qold

        self.Q_[oldstate][self.prev_action_] += self.alpha_ * deltav
        
\end{lstlisting}
\bigskip

\section{Results and discussions}
\bigskip

\subsection{Static environment}
\bigskip

In this section we benchmark Q-learning strategy using an $\epsilon-$greedy policy (POL2) and a Softmax policy (POL1) against regular $\epsilon-$greedy and Softmax policies with no strategies on the long term. 

\begin{figure}[H]
    \centering
    \subcaptionbox{P3E1000F1X5T0W1S \label{P3E1000F1X5T0W1S}}
        {\includegraphics[scale=.25]{Results/P3E1000F1X5T0W1S.png}}
    \subcaptionbox{P4E1000F1X5T0W1S \label{P4E1000F1X5T0W1S}}
       {\includegraphics[scale=.25]{Results/P4E1000F1X5T0W1S.png}}
    \subcaptionbox{P7E1000F1X5T0W1SH1POL2 \label{P7E1000F1X5T0W1SH1POL2}}
        {\includegraphics[scale=.25]{Results/P7E1000F1X5T0W1SH1POL2.png}}
    \subcaptionbox{P7E1000F1X5T0W1SH1POL1 \label{P7E1000F1X5T0W1SH1POL1}}
        {\includegraphics[scale=.25]{Results/P7E1000F1X5T0W1SH1POL1.png}}
    \caption{Q-learning strategy vs $\epsilon-$greedy and softmax in a static environment}\label{qlearning_static}
\end{figure}

The Wumpus is static and the grid has fixed size with no tore topology (borders are not connected). As we can see in Figure \ref{qlearning_static}, Q-learning is clearly outperforming both the Softmax and $\epsilon-$greedy policy. 
\par\medskip

Although Softmax gave the best results so far in a static environment the interest of Q-learning is undeniable as we clearly maximize the long term reward and take into consideration each past action and its previous reward.
\par\medskip

We tried increasing the number of flashes but things got worse for non learning policies and the gap was more evident. Let's see if this hypothesis confirms with a more complicated environment.
\bigskip

\subsection{Dynammic environment}
\bigskip

In this section we benchmarked Q-learning strategy using  an $\epsilon-$greedy policy (POL2) and a Softmax policy (POL1) against regular $\epsilon-$greedy and Softmax policies with no strategies on the long term but this type the Wumpus is randomly moving on the map.

\subsubsection{With a dynamic Wumpus}
\bigskip

\begin{figure}[H]
    \centering
    \subcaptionbox{P3E1000F1X5T0W1D \label{P3E1000F1X5T0W1D}}
        {\includegraphics[scale=.25]{Results/P3E1000F1X5T0W1D.png}}
    \subcaptionbox{P4E1000F1X5T0W1D \label{P4E1000F1X5T0W1D}}
       {\includegraphics[scale=.25]{Results/P4E1000F1X5T0W1D.png}}
    \subcaptionbox{P7E1000F1X5T0W1DH1POL2 \label{P7E1000F1X5T0W1DH1POL2}}
        {\includegraphics[scale=.25]{Results/P7E1000F1X5T0W1DH1POL2.png}}
    \subcaptionbox{P7E1000F1X5T0W1DH1POL1 \label{P7E1000F1X5T0W1DH1POL1}}
        {\includegraphics[scale=.25]{Results/P7E1000F1X5T0W1DH1POL1.png}}
    \caption{Q-learning strategy vs $\epsilon-$greedy and softmax in a dynamic environment}\label{qlearning_dynamic}
\end{figure}

As we already have guessed previously, Figure \ref{qlearning_dynamic} shows that non learning policies are better than in a static environment while Q-Learning again outperforms. With both policies the Q-learning strategies manage to find the treasure with a 100\% accuracy.
\par\medskip

We can see that the Softmax policy gives better result than the $\epsilon-$greedy. This is due to the fact that we keep exploring random actions 10\% of the time and we either meet the Wumpus or fall in the black hole.
\par\medskip

To make the game more non-deterministic with these parameters, we could enable the tore topology.

\subsubsection{With a tore topology}
\bigskip

When enabling the tore topology we have a fully stochastic environment. Figure \ref{qlearning_stochastic} shows almost no improvement for using Q-learning strategy with an $\epsilon-$greedy strategy. This is due to the fact that the optimal movement needs very steps to be found with this initial setup which gives very good results when exploiting the reawrds' history.


\begin{figure}[H]
    \centering
    \subcaptionbox{P3E1000F1X5T1W1D \label{P3E1000F1X5T1W1D}}
        {\includegraphics[scale=.25]{Results/P3E1000F1X5T1W1D.png}}
    \subcaptionbox{P4E1000F1X5T1W1D \label{P4E1000F1X5T1W1D}}
       {\includegraphics[scale=.25]{Results/P4E1000F1X5T0W1D.png}}
    \subcaptionbox{P7E1000F1X5T1W1DH1POL2 \label{P7E1000F1X5T1W1DH1POL2}}
        {\includegraphics[scale=.25]{Results/P7E1000F1X5T0W1DH1POL2.png}}
    \subcaptionbox{P7E1000F1X5T1W1DH1POL1 \label{P7E1000F1X5T1W1DH1POL1}}
        {\includegraphics[scale=.25]{Results/P7E1000F1X5T1W1DH1POL1.png}}
    \caption{Q-learning strategy vs $\epsilon-$greedy and softmax in a dynamic environment}\label{qlearning_stochastic}
\end{figure}

With a softmax policy the Q-learning algorithm confirms our assumption being the best strategy so far in a stochastic environment as this one.
\par\medskip
To confirm this assumption let's make the game far harder by adding more than one Wumpus and one hole.
\bigskip

\subsection{Increasing the game difficulty}
\bigskip

In this section we benchmarked Q-learning strategy using  an $\epsilon-$greedy policy (POL2) and a Softmax policy (POL1). With a dynamic environment and two holes.

\begin{figure}[H]
    \centering
    \subcaptionbox{P7E1000F1X5T1W1DH2POL1 \label{P7E1000F1X5T1W1DH2POL1}}
        {\includegraphics[scale=.25]{Results/P7E1000F1X5T1W1DH2POL1.png}}
    \subcaptionbox{P7E1000F1X5T1W1DH2POL2 \label{P7E1000F1X5T1W1DH2POL2}}
       {\includegraphics[scale=.25]{Results/P7E1000F1X5T1W1DH2POL2.png}}
    \subcaptionbox{P7E1000F1X5T1W2DH2POL1 \label{P7E1000F1X5T1W2DH2POL1}}
        {\includegraphics[scale=.25]{Results/P7E1000F1X5T1W2DH2POL1.png}}
    \subcaptionbox{P7E1000F1X5T1W2DH2POL2 \label{P7E1000F1X5T1W2DH2POL2}}
        {\includegraphics[scale=.25]{Results/P7E1000F1X5T1W2DH2POL2.png}}
    \caption{Q-learning strategy vs $\epsilon-$greedy and softmax with an increased difficulty}\label{qlearning_difficult}
\end{figure}

As we can see in Figure \ref{qlearning_difficult}, XXXXXXX


\subsection{Effect on the long term}
\bigskip

In this section we benchmarked Q-learning strategy using  an $\epsilon-$greedy policy (POL2) and a Softmax policy (POL1). As we can see in Figure \ref{qlearning_longterm}, XXXXXXX

\begin{figure}[H]
    \centering
    \subcaptionbox{P7E4000F4X10T1W4DH2PLO2 \label{P7E4000F4X10T1W4DH2PLO2}}
        {\includegraphics[scale=.25]{Results/P7E4000F4X10T1W4DH2PLO2.png}}
    \subcaptionbox{P7E4000F4X10T1W4DH2POL1 \label{P7E4000F4X10T1W4DH2POL1}}
       {\includegraphics[scale=.25]{Results/P7E4000F4X10T1W4DH2POL1.png}}
    \caption{Q-learning strategy vs $\epsilon-$greedy and softmax with an increased difficulty on the long term}\label{qlearning_longterm}
\end{figure}

\section{Conlusion}
\bigskip

Other policies could be implemented for the Wumpuses movement. We could either have an off-policies or an on policies method for instance to learn to avoid black-holes, or to move faster toward the agent, or try to guess where is the paper and guard. 
\par\medskip

We also don't have any information on the positions of the Wumpus (resp. the black-hole) during a smell (resp. a breeze). It could be fun to find war-game strategies and learn new ones the long term (cornering the agent or pushing him (resp. the Wumpus to flash him) in the black hole.
Although those ideas would require a massive changes in the code, it would surely be interesting to oppose different policies to see which ones converges faster in a stochastic environment and how we can learn the optimal parameters $\epsilon, \alpha, \gamma $ of the Q-learning algorithm.
\par\medskip


\bigskip
\cite{sutton2016}
\cite{sigaudbuffet}
\cite{esarsa}
\cite{csaba}
\cite{leemon}
\cite{filippi2010optimism}
\cite{storck95}
\newpage

\bibliography{bibliography.bib}{}
\bibliographystyle{plain}

\end{document}
