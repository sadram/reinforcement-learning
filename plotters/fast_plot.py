import os
from datetime import datetime
from matplotlib import pyplot as plt
from matplotlib import style


class FastPlot:
    def __init__(self):
        self.x_epis = list()
        self.y_cum_rew = list()
        self.y_avg_rew = list()
        self.cum_rew = 0
        self.y_label = 'Number of episodes'
        self.x_label = 'Cumulative reward'
        self.y_min = -500
        self.y_max = 100
        self.a_rew_color = "#2ecc71"
        self.a_rew_style = '-'
        self.a_rew_label = 'Mean reward'
        self.c_rew_color = "#d35400"
        self.c_rew_style = '.'
        self.c_rew_label = 'Cumulative reward'
        self.basepath = './results'
        self.plot_name = '_res_'
        self.plotInit()

    def plotCumRew(self):
        plt.plot(self.x_epis, self.y_cum_rew,
                 self.c_rew_style,
                 color=self.c_rew_color,
                 label=self.c_rew_label,
                 antialiased=False,
                 alpha=.3,
                 markersize=1.5)

    def plotAvgRew(self):
        plt.plot(self.x_epis, self.y_avg_rew,
                 self.a_rew_style,
                 color=self.a_rew_color,
                 label=self.a_rew_label,
                 antialiased=True,
                 linewidth=1.2)

    def plotInit(self):
        plt.clf()
        style.use(style='ggplot')
        plt.ylim(self.y_min, self.y_max)
        plt.xlabel(self.y_label)
        plt.ylabel(self.x_label)
        self.plotAvgRew()
        self.plotCumRew()
        plt.legend(loc=4)
        # plt.show()

    def save_fig(self, path, name):
        newpath = path + '/{}'.format(datetime.now().strftime('%y-%m-%d_%H-%M'))
        if not os.path.exists(newpath):
            os.makedirs(newpath)
        plt.savefig(newpath + '/{}.png'.format(name))

    def make_fig(self, name=None):
        self.save_fig(self.basepath, str(len(self.x_epis)) + self.plot_name + name)

    def print(self):
        if not self.x_epis:
            self.x_epis = [1]
            self.y_avg_rew = [self.cum_rew]
        else:
            self.y_avg_rew += [
                (self.y_avg_rew[-1] * self.x_epis[-1] + self.cum_rew) / (self.x_epis[-1] + 1)]
            self.x_epis += [self.x_epis[-1] + 1]
        self.plotAvgRew()
        self.y_cum_rew += [self.cum_rew]
        self.plotCumRew()
