from matplotlib import pyplot as plt
from matplotlib import style
from plotters.fast_plot import FastPlot


class SplitPlot(FastPlot):
    def __init__(self):
        super().__init__()
        self.plotInit()

    def plotCumRew(self):
        plt.plot(self.x_epis, self.y_cum_rew,
                 self.c_rew_style,
                 color=self.c_rew_color,
                 label=self.c_rew_label)

    def plotAvgRew(self):
        plt.plot(self.x_epis, self.y_avg_rew,
                 self.a_rew_style,
                 color=self.a_rew_color,
                 label=self.a_rew_label)

    def plotInit(self):
        plt.clf()
        style.use(style='ggplot')
        plt.ylim(self.y_min,self.y_max)
        plt.xlabel(self.y_label)
        plt.ylabel(self.x_label)
        self.plotAvgRew()
        self.plotCumRew()
        plt.legend(loc=4)
        # plt.ion()
        # plt.show()

    def make_fig(self, name=None):
        x_epis_len = len(self.x_epis)
        if x_epis_len % 100 == 0:
            super().save_fig(self.basepath, str(len(self.x_epis)) + self.plot_name + str(name))

    def print(self):
        super().print()
        self.make_fig()


