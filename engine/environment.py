import numpy as np
import random

from helpers.actions import Action


class Environment:
    def __init__(self, agent, setup=None):
        self.hole_pos_ = list()
        self.n_flash_ = None
        self.wumpus_pos_ = None
        self.valid_pos_ = None

        self.grid_size_ = setup.size, setup.size

        self.treasure_pos_ = (self.grid_size_[0] - 1, self.grid_size_[1] - 1)
        self.agent = agent


        self.DEFAULT_N_FLASH = setup.flash
        self.TORE_TOPO = setup.tore
        self.DYN_WUMPUS = setup.dyn_wumpus
        self.NUM_WUMPUS = setup.num_wumpus
        self.NUM_HOLES = setup.num_holes

        # Rewards
        self.DEFAULT_REWARD = -1.
        self.KILL_REWARD = 5.
        self.TREASURE_REWARD = 100.
        self.HOLE_REWARD = -10.
        self.WUMPUS_REWARD = -10.
        self.WINGAME = False

        self.reset()

    def place_wumpuses(self):
        self.wumpus_pos_ = list()
        for i in range(self.NUM_WUMPUS):
            # for the sake of simplicity all Wumpuses start from the same initial position
            # but then they came move randomly
            # TODO: make them random and pop them out for the valid_pos_
            self.wumpus_pos_ += [(1, 2)]

    def place_holes(self):
        # remove agent's position from the valid positions
        # TODO: don't hardcode it and make it random
        if (0, 0) in self.valid_pos_:
            self.valid_pos_.remove((0, 0))

        # remove treasure's position from the valid positions
        # TODO: make the treasure position setable
        if self.treasure_pos_ in self.valid_pos_:
            self.valid_pos_.remove(self.treasure_pos_)

        # remove wumpuses' positions from the valid positions
        # TODO: only one position but keep the loop for further use
        for wumpus in self.wumpus_pos_:
            if wumpus in self.valid_pos_:
                self.valid_pos_.remove(wumpus)

        # place first hole at the original position and remove it from the valid positions
        self.hole_pos_ += [(1, 1)]
        self.valid_pos_.remove((1, 1))

        # place all the other holes and remove them from the valid positions
        for h in range(0, self.NUM_HOLES - 1):
            next_hole = random.choice(self.valid_pos_)
            self.hole_pos_ += [next_hole]
            self.valid_pos_.remove(next_hole)

    def reset(self):
        self.agent.reset()
        self.WINGAME = False
        self.treasure_pos_ = (self.grid_size_[0] - 1, self.grid_size_[1] - 1)
        init_state = self.getInitState()
        self.agent.nextState(init_state, 0.)
        # TODO: keep it her for the moment might be usefull afterwards
        self.valid_pos_ = [(i, j) for i in range(self.grid_size_[0]) for j in range(self.grid_size_[1])]
        self.place_wumpuses()
        self.place_holes()

    def getInitState(self):
        return [0, 0, 0, 0, self.DEFAULT_N_FLASH]

    def getGridSize(self):
        return self.grid_size_

    def getWumpusPosition(self):
        return self.wumpus_pos_

    def getHolePosition(self):
        return self.hole_pos_

    def getTreasurePosition(self):
        return self.treasure_pos_

    def getNFlash(self):
        return self.n_flash_

    def moveWumpus(self):
        for i in range(self.NUM_WUMPUS):
            a = Action(np.random.randint(1, 5))  # Warning hardcoded value!
            self.wumpus_pos_[i] = self.moveAgent(self.wumpus_pos_[i], a)

    def moveAgent(self, curr_pos, a):
        next_pos = []
        if a == Action.UP:
            next_pos = [curr_pos[0], curr_pos[1] + 1]
        elif a == Action.DOWN:
            next_pos = [curr_pos[0], curr_pos[1] - 1]
        elif a == Action.LEFT:
            next_pos = [curr_pos[0] - 1, curr_pos[1]]
        elif a == Action.RIGHT:
            next_pos = [curr_pos[0] + 1, curr_pos[1]]

        if not self.TORE_TOPO:
            next_pos = [min(self.grid_size_[0] - 1, next_pos[0]), min(self.grid_size_[1] - 1, next_pos[1])]
            next_pos = [max(0, next_pos[0]), max(0, next_pos[1])]
        else:
            if next_pos[0] == self.grid_size_[0]:
                next_pos = [0, next_pos[1]]
            elif next_pos[0] == -1:
                next_pos = [self.grid_size_[0] - 1, next_pos[1]]

            if next_pos[1] == self.grid_size_[1]:
                next_pos = [next_pos[0], 0]
            elif next_pos[1] == -1:
                next_pos = [next_pos[0], self.grid_size_[1] - 1]

        return next_pos

    def flashAgent(self, s, a, pos):
        agent_pos = s[:2]
        n_flash = s[4]
        if n_flash > 0 and self.wumpus_pos_[pos][0] >= 0:
            if a == Action.FLASH_UP:
                if self.wumpus_pos_[pos][0] == agent_pos[0] and self.wumpus_pos_[pos][1] == agent_pos[1] + 1:
                    self.wumpus_pos_[pos] = [-1, -1]
                    return True
            elif a == Action.FLASH_DOWN:
                if self.wumpus_pos_[pos][0] == agent_pos[0] and self.wumpus_pos_[pos][1] == agent_pos[1] - 1:
                    self.wumpus_pos_[pos] = [-1, -1]
                    return True
            elif a == Action.FLASH_LEFT:
                if self.wumpus_pos_[pos][0] == agent_pos[0] - 1 and self.wumpus_pos_[pos][1] == agent_pos[1]:
                    self.wumpus_pos_[pos] = [-1, -1]
                    return True
            elif a == Action.FLASH_RIGHT:
                if self.wumpus_pos_[pos][0] == agent_pos[0] + 1 and self.wumpus_pos_[pos][1] == agent_pos[1]:
                    self.wumpus_pos_[pos] = [-1, -1]
                    return True
        return False

    def testForEnd(self, s):
        agent_pos = s[:2]
        if self.wumpus_pos_[0] == agent_pos[0] and self.wumpus_pos_[1] == agent_pos[1]:
            return self.WUMPUS_REWARD, True
        elif self.hole_pos_[0] == agent_pos[0] and self.hole_pos_[1] == agent_pos[1]:
            return self.HOLE_REWARD, True
        elif self.treasure_pos_[0] == agent_pos[0] and self.treasure_pos_[1] == agent_pos[1]:
            self.WINGAME = True
            return self.TREASURE_REWARD, True
        else:
            return 0, False

    def updateSense(self, agent_pos):
        [smell, breeze] = [0, 0]
        for wumpus_pos in self.wumpus_pos_:
            if abs(wumpus_pos[0] - agent_pos[0]) + abs(wumpus_pos[1] - agent_pos[1]) < 2:
                smell = 1
        for hole_pos in self.hole_pos_:
            if abs(hole_pos[0] - agent_pos[0]) + abs(hole_pos[1] - agent_pos[1]) < 2:
                breeze = 1
        return [smell, breeze]

    def nextState(self):
        a = self.agent.getAction()
        s = self.agent.getState()
        reward = self.DEFAULT_REWARD

        next_agent_pos = s[:2]
        n_flash = s[-1]
        if a < 5:
            next_agent_pos = self.moveAgent(s[:2], a)
        else:
            if n_flash > 0:
                n_flash -= 1
                for pos in range(self.NUM_WUMPUS):
                    flash_success = self.flashAgent(s, a, pos)
                    if flash_success:
                        reward += self.KILL_REWARD

        sense = self.updateSense(next_agent_pos)
        new_state = next_agent_pos + sense + [n_flash]
        (end_reward, end_flag) = self.testForEnd(new_state)

        if self.DYN_WUMPUS:
            self.moveWumpus()

        return new_state, a, reward + end_reward, end_flag
