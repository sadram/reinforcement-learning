from collections import defaultdict

from engine.environment import Environment
from engine.selectors import selectPolicy
from plotters.split_plot import SplitPlot, FastPlot
import math


class RLPlatform(FastPlot):
    def __init__(self, setup):
        super().__init__()
        self.LOGGER_TIME_STEP = setup.verbose
        self.agent = selectPolicy(setup.policy)
        self.reset()
        self.environment = Environment(self.agent, setup)
        self.agent_prev_pos = self.agent.getPosition()
        self.time_step_ = 0
        self.cum_rew = 0

        self.num_games = setup.episodes
        self.num_flash = setup.flash
        self.grid_size = setup.size
        self.policy_name = setup.policy
        self.is_tore = setup.tore
        self.is_dyn_wumpys = setup.dyn_wumpus

        self.lose_str = 'Lost'
        self.win_str = 'Won'
        self.game_status = None
        self.game_count = None
        self.init()

        # EPISODES(int from plotter)|POLICY(int)|FLASH(int)|GRID_SIZE(int)|TORE(Bool)|DYN_WUMPUS(B)
        self.game_name = 'P' + str(self.policy_name) + \
                         'F' + str(self.num_flash) + \
                         'X' + str(self.grid_size) + \
                         'T' + str(int(self.is_tore)) + \
                         'W' + str(int(self.is_dyn_wumpys))

        self.beta = 100
        self.C_ij = dict()
        self.state_cnt = dict()

    def init(self):
        initstats = lambda: defaultdict(initstats)
        self.game_count = initstats()
        self.game_count[self.win_str] = 0
        self.game_count[self.lose_str] = 0

    def reset(self):
        self.time_step_ = 0
        self.cum_rew = 0

    def updateLoop(self):
        self.agent_prev_pos = self.agent.getPosition()
        prev_state = self.agent.getState()
        (new_state, a, reward, ended) = self.environment.nextState()
        self.agent.nextState(new_state, reward)

        self.dynamic_reward_divergence(prev_state, reward, new_state)

        self.time_step_ += 1
        self.cum_rew += reward
        if self.LOGGER_TIME_STEP:
            print("time step " + str(self.time_step_) + " " + str(prev_state) + " " + str(a) + " " + str(
                self.agent.getState()) + " " + str(self.cum_rew))

        if ended:
            self.print()
            self.game_status = [self.lose_str, self.win_str][self.environment.WINGAME]
            self.game_count[self.game_status] += 1
            if not self.LOGGER_TIME_STEP:
                print("{} game ({} steps): "
                      "\n\t -> Reward(cumul, avg) = ({}, {})"
                      "\n\t -> (win, loss) = ({}, {})"
                      "\n\t -> final state {}"
                      .format(self.game_status,
                              self.time_step_,
                              self.cum_rew,
                              round(self.y_avg_rew[-1], 6),
                              self.game_count[self.win_str], self.game_count[self.lose_str],
                              self.agent.getState()))
            else:
                print("{} in {} steps; c_rew, a_rew = {}, {}; w,l = {}, {}"
                      .format(self.game_status, self.time_step_, self.cum_rew, round(self.y_avg_rew[-1], 6),
                              self.game_count[self.win_str], self.game_count[self.lose_str]))
            if self.num_games == len(self.x_epis):
                self.make_fig(self.game_name)
            self.reset()
            self.environment.reset()
        return True == ended

    def dynamic_reward_divergence(self, state_cur, reward, state_next, beta=50):

        # init first visit
        c_ij = str(state_cur)
        if c_ij not in self.C_ij.keys():
            Delta_entropy = 0;
            self.state_cnt[c_ij] = 1
            self.C_ij[c_ij] = {}
            self.C_ij[c_ij][str(state_next)] = 1
            Entr_next = [0]

        # compute the dynamic reward using the Kullback Leiber divergence
        else:
            Entr_cur = [
                -self.C_ij[c_ij][state] / self.state_cnt[c_ij] * math.log(self.C_ij[c_ij][state] / self.state_cnt[c_ij], 2)
                for state in self.C_ij[c_ij].keys()
                ]

            self.state_cnt[c_ij] += 1

            # first time we see the new state, we initialise its transition counter to 1
            if str(state_next) not in self.C_ij[c_ij].keys():
                self.C_ij[c_ij][str(state_next)] = 1
            # else increment it
            else:
                self.C_ij[c_ij][str(state_next)] += 1

            Entr_next = [
                -self.C_ij[c_ij][state] / self.state_cnt[c_ij] * math.log(self.C_ij[c_ij][state] / self.state_cnt[c_ij], 2) \
                           for state in self.C_ij[c_ij].keys()
                ]
            Delta_entropy = abs(sum(Entr_next) - sum(Entr_cur))

        Dynamic_reward = reward * (1 - beta * min(Delta_entropy, 1))

        self.agent.nextState(state_next, Dynamic_reward)
