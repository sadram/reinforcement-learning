from agents.agent import Agent
from agents.custom_policy import CustomPolicy
from agents.egreedy_policy import EpslionGreedyPolicy
from agents.greedy_policy import GreedyPolicy
from agents.qlearning_policy import QLearningPolicy
from agents.softmax_policy import SoftmaxPolicy
from agents.ucb_policy import UpperConfidenceBoundPolicy
# from agents import *


def selectPolicy(value):
    if value == 1:
        return Agent()
    elif value == 2:
        return GreedyPolicy()
    elif value == 3:
        return EpslionGreedyPolicy()
    elif value == 4:
        return SoftmaxPolicy()
    elif value == 5:
        return UpperConfidenceBoundPolicy()
    elif value == 6:
        return CustomPolicy()
    elif value == 7:
        return QLearningPolicy()
    else:
        Agent()
