from tkinter import Tk, Canvas, NW

from PIL import Image, ImageTk

from engine.selectors import selectPolicy
from engine.environment import Environment
from plotters.split_plot import SplitPlot
# from plotters.plotter import Plot


class WumpusHMI(Tk, SplitPlot):
    def __init__(self, setup=None):
        Tk.__init__(self)
        SplitPlot.__init__(self)

        # Attributes placeholders
        self.image_wumpus = None
        self.image_hole = None
        self.image_treasure = None
        self.image_hunter = None
        self.time_step_ = None
        self.cum_rew = None

        self.title("Wumpus world")
        self.DELTA_TIME = 1
        self.IMG_SIZE = 150
        self.LOGGER_TIME_STEP = setup.verbose
        self.agent = selectPolicy(setup.policy)
        self.reset()
        self.environment = Environment(self.agent, setup)
        self.agent_prev_pos = self.agent.getPosition()
        self.wumpus_prev_pos = self.environment.getWumpusPosition()
        self.loadImages()
        self.createWorld()

    def loadImages(self):
        image = Image.open("./img/wumpus.gif")
        image = image.resize((self.IMG_SIZE, self.IMG_SIZE), Image.ANTIALIAS)
        self.image_wumpus = ImageTk.PhotoImage(image)

        image = Image.open("./img/black.gif")
        image = image.resize((self.IMG_SIZE, self.IMG_SIZE), Image.ANTIALIAS)
        self.image_hole = ImageTk.PhotoImage(image)

        image = Image.open("./img/papers.gif")
        image = image.resize((self.IMG_SIZE, self.IMG_SIZE), Image.ANTIALIAS)
        self.image_treasure = ImageTk.PhotoImage(image)

        image = Image.open("./img/einstein.gif")
        image = image.resize((self.IMG_SIZE, self.IMG_SIZE), Image.ANTIALIAS)
        self.image_hunter = ImageTk.PhotoImage(image)

    def convertCoord(self, pos):
        grid_size = self.environment.getGridSize()
        return grid_size[1] - pos[1] - 1, pos[0]

    def createWorld(self):
        for line in range(self.environment.getGridSize()[0]):
            for col in range(self.environment.getGridSize()[1]):
                canvas = Canvas(self, width=self.IMG_SIZE, height=self.IMG_SIZE, bg='grey')

                for wump_pos in (self.environment.getWumpusPosition()):
                    if (line, col) == self.convertCoord(wump_pos):
                        canvas.create_image(0, 0, anchor=NW, image=self.image_wumpus)

                for hole_pos in (self.environment.getHolePosition()):
                    if (line, col) == self.convertCoord(hole_pos):
                        canvas.create_image(0, 0, anchor=NW, image=self.image_hole)

                if (line, col) == self.convertCoord(self.environment.getTreasurePosition()):
                    canvas.create_image(0, 0, anchor=NW, image=self.image_treasure)

                if (line, col) == self.convertCoord(self.agent.getPosition()):
                    canvas.create_image(0, 0, anchor=NW, image=self.image_hunter)
                canvas.grid(row=line, column=col)

    def updateWorld(self):
        for line in range(self.environment.getGridSize()[0]):
            for col in range(self.environment.getGridSize()[1]):

                if (line, col) == self.convertCoord(self.agent.getPosition()):
                    canvas = Canvas(self, width=self.IMG_SIZE, height=self.IMG_SIZE, bg='grey')
                    canvas.create_image(0, 0, anchor=NW, image=self.image_hunter)
                    canvas.grid(row=line, column=col)

                elif (line, col) == self.convertCoord(self.agent_prev_pos[:2]):
                    canvas = Canvas(self, width=self.IMG_SIZE, height=self.IMG_SIZE, bg='grey')

                    for wump_pos in (self.environment.getWumpusPosition()):
                        if (line, col) == self.convertCoord(wump_pos):
                            canvas.create_image(0, 0, anchor=NW, image=self.image_wumpus)

                    for hole_pos in (self.environment.getHolePosition()):
                        if (line, col) == self.convertCoord(hole_pos):
                            canvas.create_image(0, 0, anchor=NW, image=self.image_hole)

                    if (line, col) == self.convertCoord(self.environment.getTreasurePosition()):
                        canvas.create_image(0, 0, anchor=NW, image=self.image_treasure)
                    canvas.grid(row=line, column=col)

                wumpus_list = self.environment.getWumpusPosition()
                for i in range(len(wumpus_list)):
                    wumpus_cur = wumpus_list[i]
                    wumpus_pos_prev = self.wumpus_prev_pos[i]
                    if self.environment.DYN_WUMPUS or (wumpus_pos_prev[0] == -1 and wumpus_cur[0] > -1):
                        if (line, col) == self.convertCoord(wumpus_cur):
                            canvas = Canvas(self, width=self.IMG_SIZE, height=self.IMG_SIZE, bg='grey')
                            canvas.create_image(0, 0, anchor=NW, image=self.image_wumpus)
                            canvas.grid(row=line, column=col)

                        elif (line, col) == self.convertCoord(wumpus_pos_prev):
                            canvas = Canvas(self, width=self.IMG_SIZE, height=self.IMG_SIZE, bg='grey')

                            hole_pos_list = self.environment.getHolePosition()
                            for hole_pos in hole_pos_list:
                                if (line, col) == self.convertCoord(hole_pos):
                                    canvas.create_image(0, 0, anchor=NW, image=self.image_hole)

                            if (line, col) == self.convertCoord(self.environment.getTreasurePosition()):
                                canvas.create_image(0, 0, anchor=NW, image=self.image_treasure)
                            canvas.grid(row=line, column=col)


    def reset(self):
        self.time_step_ = 0
        self.cum_rew = 0

    def updateLoop(self):
        self.agent_prev_pos = self.agent.getPosition()
        self.wumpus_prev_pos = self.environment.getWumpusPosition()
        prev_state = self.agent.getState()
        (new_state, a, reward, end_flag) = self.environment.nextState()
        self.agent.nextState(new_state, reward)

        self.time_step_ += 1
        self.cum_rew += reward
        if self.LOGGER_TIME_STEP:
            print("time step " + str(self.time_step_) + " " + str(prev_state) + " " + str(a) + " " + str(
                self.agent.getState()) + " " + str(self.cum_rew))

        if end_flag:
            self.print()
            self.reset()
            self.environment.reset()

        self.updateWorld()
        self.after(self.DELTA_TIME, self.updateLoop)
