\documentclass[paper=a4, fontsize=11pt]{scrartcl}
\usepackage[T1]{fontenc} 
\usepackage[english]{babel} 
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsthm}
\usepackage{fancyhdr} 
\usepackage{sectsty}
\usepackage{float}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[]{hyperref}
\usepackage{listings}
\usepackage[framed]{matlab-prettifier}

\allsectionsfont{\normalfont \bfseries}
\pagestyle{fancyplain}
\fancyhead{}
\fancyfoot[L]{}
\fancyfoot[C]{\thepage}
\fancyfoot[R]{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\setlength{\headheight}{14pt}
\setlength\parindent{0pt} 

\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal rule command with 1 argument of height

\title{	
    \normalfont 
    \text{Ecole Centrale Paris - MSc Data Science } \\ [20pt]
    \horrule{2pt} \\[0.5cm] 
    \huge \bfseries{Deep Learning Assignment 3} \\ 
    \horrule{2pt} \\[0.5cm]
}
\author{Julien Mardas}
\date{September 14, 2016}

\begin{document}
\maketitle

%----------------------------------------------------------------------------------------
%	Analytical exercise
%----------------------------------------------------------------------------------------

\section{Analytical exercise}

Given a training set X of M samples, $X = {x^1, ..., x^M}$, and the observable variable vector $Y= {y^1, ..., y^M}$, with $N+K = M$, such that:
\par\medskip

$y = \begin{bmatrix}
x^{(N)} \\
h^{(K)}
\end{bmatrix}$ 
\par\medskip

we want to find an expression of the gradient of the log-likelihood $S(X,Y)$ with respect to the interconnection weights of the Boltzmann machine $W_{k,m}$. 
\par\medskip

We want to find this expression in order to estimate the weight matrix $W$ representing the dependencies between hidden and visible layers. As to maximize the log-likelihood we compute its gradient:
\par\medskip

\begin{align} 
    S(X,W)  &= \sum_{m=1}^{M} \log P(x^m;W) \\
            &= \sum_{m=1}^{M} \log \sum_{h} \frac{1}{Z} \exp (-E(x^m,h;W))) \\
            &= \sum_{m=1}^{M} \log \left (\sum_{h} \exp (-E(x^m,h;W)) \right) - \log(Z) \\
\end{align}
Where $Z = \sum_{x,h} \exp(-E(x,h;W))$. Therefor, we can express the gradient of the likelihood as:
\par\medskip

\begin{align} 
    \frac{\partial S(X,W)}{\partial W_{k,l}} 
        &= \frac{\partial }{\partial W_{k,l}} \left ( \sum_{m=1}^{M} \log \left (\sum_{h} \exp (-E(x^m,h;W)) \right) - \log(Z) \right) \\
        %
        &= \frac{\partial }{\partial W_{k,l}} \sum_{m=1}^{M} \log \left (\sum_{h} \exp (-E(x^m,h;W)) \right) - \frac{\partial \log Z}{\partial W_{k,l}} 
\end{align}
\par\medskip

Let's compute the gradient of each term. \newline
Let,
\begin{align*}
    A(x^m,h;W) 
        &= \log \left (\sum_{h} \exp (-E(x^m,h;W)) \right) \\
    D(Z) 
        &= \log(Z)
\end{align*}
\par\medskip

Then,
\begin{align} 
   \frac{\partial }{\partial W_{k,l}} A(x^m,h;W)   
        &= \frac{\partial }{\partial W_{k,l}} \sum_{m=1}^{M} \log \left (\sum_{h} \exp (-E(x^m,h;W)) \right) \\
        &= \frac
            {\sum_{h} \left (\ - \frac{\partial E(x^m,h;W)}{\partial W_{k,l}}\ .\ \exp (-E(x^m,h;W)) \right)} 
            {\sum_{h} \exp (-E(x^m,h;W))}
\end{align}
\par\medskip

The energy of the Boltzmann Machine is defined by:
\begin{align*}
    E_{BM}(y;W)  &= -\frac{1}{2}y^TWy \\
    E_{BM}(x,h;W)&= -\frac{1}{2}x^TW_{x,x}x-x^TW_{x,h}y-\frac{1}{2}y^TW_{h,h}xy
\end{align*}
\par\medskip

With, $W = \begin{bmatrix}
 W_{x,x} & W_{x,h} \\ 
 W_{x,h} & W_{h,h} 
\end{bmatrix}$ symmetric and definite
\par\medskip

As a consequence, 
\begin{align*}
   -\frac{\partial E(x^m,h;W)}{\partial W_{k,l}}  &= \frac{\partial }{\partial W_{k,l}}\frac{1}{2}y^TWy \\
    &= \frac{1}{2} y_k y_l +\frac{1}{2} y_l y_k = y_k y_l
\end{align*}
\par\medskip

Using this result in the equality (9), we have:
\begin{align}
    A(x^m,h;W)   &= \frac
                {\sum_{h} y_k y_l  \exp (-E(x^m,h;W))}
                {\sum_{h} \exp (-E(x^m,h;W))}
\end{align}
\par\medskip

Using the marginalization law under the Boltzmann-Gibbs distribution we have, 
\begin{align}
    P(x,h;W) &= \sum_h \frac{1}{Z}\exp(-E(x,h;W)) \\
    P(h|x^m;W) &= \frac{\exp(-E(x^m,h;W)}{\sum_{h} \exp(-E(x^m,h;W)}
\end{align}
\par\medskip

Then, equation (10) becomes:
\begin{align}
    A(x^m,h;W)   &=\ \langle y_k \, y_l \rangle_{P(x^m,h;W)}
\end{align}
\par\medskip

Knowing that $Z = \sum_{x,h}\exp(-E(x,h;W)$ and equation (11), let's calculate the second term $D(Z)$ taking back equation (6):
\begin{align}
    \frac{\partial D(Z)}{\partial W_{k,l}}  
        &= \frac{\sum_{x,h} y_k y_l exp(-Ex,h;W))}{Z} \\
        &= \sum_{x,h} y_k y_l  P(x,h;W) \\
        &= \ \langle y_k \, y_l \rangle_{P(x,h;W)}
\end{align}
\par\medskip

Finally, we use (13) and (16) in (5) and we obtain:
\begin{align}
 \frac{\partial S(X,W)}{\partial W_{k,l}} 
        &= \sum_{m=1}^{M} 
            \left[
                \langle y_k \, y_l \rangle_{P(x^m,h;W)}\  - %awake
                \langle y_k \, y_l \rangle_{P(x,h;W)}       %dream
            \right] \qed
\end{align}
\par\medskip



%----------------------------------------------------------------------------------------
%	EXACT SUMMATION
%----------------------------------------------------------------------------------------

\section{Exact summation}
We want to approximate the generated Hinton diagram that is built randomly as shown in figure \ref{hiton_diag_orig}. To do so, we will use three models:
\begin{itemize}
    \item the Ising model (IM, no interaction between observable and/or hidden variables)
    \item the Boltzmann Machine (BM)
	\item the Restricted Boltzmann Machine (RBM, no interactions between hidden variables) 
\end{itemize}
We will implement a gradient ascend found in the equation (16) to maximize the log-likelihood of our data with respect to the parameter W that we want to estimate.
\par\medskip

We initialize the random generator seed to zero to be able to compare the three models.
\begin{figure}[H]
    \centering
    \includegraphics[scale=.9]{Results/Hinton_500x4.png}
    \caption{Original Hinton diagram: showbar(genbars(500,4))}
    \label{hiton_diag_orig}
\end{figure}
\par\medskip

Using equation (16), we have to terms in the sum that are defined below:
\begin{itemize}
    \item The awake part of the energy (must be calculated for each observation m)
    \item The dream part of the energy (independent of m)
\end{itemize}

\begin{align*}
 E_{awake}(x^m,h;W) &=  \langle y_k \, y_l \rangle_{P(x^m,h;W)}\\   %awake% 
 E_{dream}(x,h;W)   &=  \langle y_k \, y_l \rangle_{P(x,h;W)}       %dream%
\end{align*}
\par\medskip

The evolution of the log-likelihood and the corresponding density estimation obtained after 200 and 2000 iterations of gradient ascent are shown in figure \ref{loglikelihood_rbm_im}.
\begin{figure}[H]
    \centering
    \subcaptionbox{ \label{it200_part1_log_likelihood}}
        {\includegraphics[scale=.45]{Results/it200_part1_ll.png}}
    \subcaptionbox{ \label{it2000_part1_log_likelihood}}
       {\includegraphics[scale=.45]{Results/it2000_part1_ll.png}}
    \subcaptionbox{IM \label{it2000_part1_bar_IM}}
        {\includegraphics[scale=.45]{Results/it2000_part1_bar_IM.png}}
    \subcaptionbox{RBM \label{it2000_part1_bar_RBM}}
        {\includegraphics[scale=.45]{Results/it2000_part1_bar_RBM.png}}    
    \subcaptionbox{BM \label{it2000_part1_bar_BM}}
        {\includegraphics[scale=.45]{Results/it2000_part1_bar_BM.png}}
    \subcaptionbox{Original \label{it2000_part1_bar_original}}
        {\includegraphics[scale=.45]{Results/it2000_part1_bar_original.png}}
    \caption{Evolution of the log-likelihoods and estimations for the IM, BM and RBM}\label{loglikelihood_rbm_im}
\end{figure}
\par\medskip

With 200 iterations we could think the Ising Model is the best model that is the closest one to the Boltzmann Machine -- the Restricted Boltzmann Machine giving unsatisfactory results. 
\par\medskip

But after 500 iterations, the RBM outperforms IM and is closer to the original distribution. We can clearly see that the training of the network gives better results when the hidden variables are activated. We could say that before 500 iterations a BM is equivalent to the IM since we only make use of the visible layer of the network and there are no mutual interactions between hidden and visible units. 
\par\medskip

This assumption is corroborated by Figure \ref{it2000_part1_network_precision} where we clearly see an unsatisfactory precision of the network for all models.
But when the hidden units are ready to be used, the RBM is more accurate than the IM: both BM and RBM's precision respectively converge to 0.9 and 1 but IM tends to 0.7 (Figure \ref{it2000_part1_network_precision}). This is because IM does not make use of hidden variables whereas RBM and BM do.

\begin{figure}[H]
    \centering
    \subcaptionbox{ \label{it200_part1_network_precision}}
        {\includegraphics[scale=.45]{Results/it200_part1_network_precision.png}}
    \subcaptionbox{ \label{it2000_part1_network_precision}}
        {\includegraphics[scale=.45]{Results/it2000_part1_network_precision.png}}
    \caption{Evolution of the Network precision}\label{part1_network_precision}
\end{figure}
\par\medskip

%----------------------------------------------------------------------------------------
%	Block-Gibbs sampling and Contrastive Divergence
%----------------------------------------------------------------------------------------
\newpage

\section{Block-Gibbs sampling and Contrastive Divergence}
We will now implement Block-Gibbs sampling with a Restricted Boltzmann Machine to approximate more accurately the original distribution.
\par\medskip

The block Gibbs sampling will take $x^0, W, L$ as input and output $x^L, P(h|x^L;W) $ with 8 hidden variables ($M=8$). We will train the RBM with Contrastive Divergence, $L=1$ and $L=10$ and compare the log-likelihood to the one obtained with the RBM using brute-force enumeration.
\par\medskip

Following \url{http://www.cs.toronto.edu/~rsalakhu/code_AIS/rbm.m}, we implemented the contrast divergence using the following model: 

\begin{align*}
    E_{dream}(x,h;W)   
        &=  \langle y_k \, y_l \rangle_{P(x,h;W)} \\
        &\simeq \sum_{m=1}^{M} y_{k}^{m,l} (2P(h_l|x^{m,L};W-1)
\end{align*}
The code of the Block-Gibbs sampling can be found in listing 1.
\par\medskip

Similarly as without Gibbs sampling, after 200 iterations all models fail to approximate the distribution with a precision below 50\% (\ref{it200_part2_log_likelihood}).
The learning rate has been set to 0.1 to increase the convergence on the algorithm as the default learning rate (0.05) gave bad results. 
\begin{figure}[H]
    \centering
    \subcaptionbox{ \label{it200_part2_log_likelihood}}
        {\includegraphics[scale=.45]{Results/it200_part2_ll.png}}
    \subcaptionbox{ \label{it2000_part2_log_likelihood}}
       {\includegraphics[scale=.45]{Results/it2000_part2_ll.png}}
    \subcaptionbox{RBM-BF \label{it2000_part2_bar_RBM_BF}}
        {\includegraphics[scale=.45]{Results/it2000_part2_bar_RBM_BF.png}}
    \subcaptionbox{RBM \label{it2000_part2_bar_RBM_CD_1}}
        {\includegraphics[scale=.45]{Results/it2000_part2_bar_RBM_CD_1.png}}    
    \subcaptionbox{BM \label{it2000_part2_bar_RBM_CD_10}}
        {\includegraphics[scale=.45]{Results/it2000_part2_bar_RBM_CD_10.png}}
    \subcaptionbox{Original \label{it2000_part2_bar_original}}
        {\includegraphics[scale=.45]{Results/it2000_part2_bar_original.png}}
    \caption{Log-likelihoods and estimations RBM-BF, RBM-CD-1 and RBM-CD2}
    \label{loglikelihood_rbm_cd1_10}
\end{figure}
\par\medskip

We clearly see \ref{loglikelihood_rbm_cd1_10} that the actuel setup is giving very good results.
\par\medskip

\begin{figure}[H]
    \centering
    \subcaptionbox{ \label{it200_part2_network_precision}}
        {\includegraphics[scale=.45]{Results/it200_part2_network_precision.png}}
    \subcaptionbox{ \label{it2000_part2_network_precision}}
        {\includegraphics[scale=.45]{Results/it2000_part2_network_precision.png}}
    \caption{Evolution of the Network precision}\label{part2_network_precision}
\end{figure}
\par\medskip

Gibbs sampling with Contrastive Divergence gave better results that now converge faster and better and we find the true distribution with a precision of 1. In fact, the bigger L is the more independent from the initial observations and the more our approximation is fine tuned in our case.
\par\medskip

However, with a more complex case this won't work. KyungHyun Cho, Tapani Raiko and Alexander Ilin advised using an adaptive learning and enhanced gradient \cite{kyuncho} for training an RBM.

\newpage
\lstset{
  style              = Matlab-editor,
  basicstyle         = \mlttfamily,
  escapechar         = ",
  mlshowsectionrules = true,
}
\begin{lstlisting}[captionpos=b, caption = {Block-Gibbs sampling code} \label{code_gibbs}]{block_gibbs.m}
function [x_out, P_h]  = block_gibbs(x_in, W, L)
%
% block_gibbs(obs_K,W_inter,K)
%
%   Performs Block-Gibbs sampling 
%   using x^0, W, L as input 
%   outputing x^L, P(h|x^L;W) with 8 hidden variables (M=8) 
%
%  Arguments: (obs_K, W_inter, K)
%   x_in:       observed variable
%   W:          parameter of the model for the given iteration
%   L:          contrastive divergence
%
%  Returns: (obs_K, P_hidden_K)
%   x_out:      sample variable
%   P_h: probability of hidden variable to be 1

    x_out = x_in;
    h = zeros(size(W,2),1);

    for i = 1:L
        % Draw h^k from x^{k-1}
        p1      =   1./(1+exp(-2*x_out*W));
        h       =   2*(p1'>rand(size(h,1),1))-1 ;
        
        P_h     =   1./(1+exp(-2*x_out*W));
        
        % Draw x^k from h^k
        p2      =   1./(1+exp(-2*W*h));
        x_out   =   2*(p2'>rand(1,size(x_in,2)))-1 ;

    end
    
    P_h = 1./(1+exp(-2*x_out*W));
end
\end{lstlisting}

\bibliography{bibliography.bib}{}
\bibliographystyle{plain}
\makeatletter
\renewcommand\@biblabel[1]{}
\makeatother
\cite{vedaldi15matconvnet}


\end{document}l