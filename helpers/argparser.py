import argparse


def str2bool(b):
    ans = b.lower()
    if ans not in ("yes", "true", "t", "1", "no", "false", "f", "0"):
        raise argparse.ArgumentTypeError("%s is not a boolean value" % b)
    return ans in ("yes", "true", "t", "1")


def parse_args():
    # Retrieve the arguments from the command-line
    parser = argparse.ArgumentParser(prog='Wumpus',
                                     description="Wumpus game implementation" +
                                                 "for testing reinforcement learning algorithms")
    # redefine the boolean type
    parser.register('type', 'bool', str2bool)

    group = parser.add_mutually_exclusive_group()

    # Program arguments
    parser.add_argument('--version', action='version', version="%(prog)s 1.0")
    group.add_argument('-v', '--verbose', action='store_true', help='increase output verbosity')
    group.add_argument('-q', '--quiet', action='store_true', help='reduce output')

    # Game configuration
    parser.add_argument('-g', '--gui', type='bool', default=False, metavar='B',
                        help='activate the graphical user interface (default: true)')  # if True infinite Loop?
    parser.add_argument('-t', '--tore', type='bool', default=True, metavar='B',
                        help='activate a tore grid (default: true)')
    parser.add_argument('-s', '--size', type=int, choices=range(3, 21), default=4,
                        help='integer defining the grid size {3..20} (default: 4x4)', metavar='N')
    parser.add_argument('-e', '--episodes', type=int, choices=range(1, 10001), default=100,
                        help='integer defining the maximum number of episodes per game {0..100} (default: 100)',
                        metavar='N')

    # Player setup
    parser.add_argument('-w', '--dyn_wumpus', type='bool', default=False, metavar='B',
                        help='activate move for the Wumpus (default: false)')
    parser.add_argument('-W', '--num_wumpus', type=int, choices=range(1, 5), default=1,
                        help='number of Wumpus(es) on the grid {1..4} (default: 1)',
                        metavar='N')
    parser.add_argument('-o', '--num_holes', type=int, choices=range(1, 5), default=1,
                        help='number of hole(s) on the grid {1..4} (default: 1)',
                        metavar='N')
    parser.add_argument('-f', '--flash', type=int, choices=range(0, 101), default=2,
                        help='integer defining the numbers of flashes to kill the wumpus {0..100} (default: 2)',
                        metavar='N')
    parser.add_argument('-p', '--policy', type=int, choices=range(1, 8), default=1,
                        help="integer defining the policy of the agent: " +
                             "Random(1 default), Greedy(2), e-Greedy(3), SoftMax(4), UCB(5), Custom(6), Qlearning(7)",
                        metavar='N')

    return parser.parse_args()
