from agents.agent import Agent
from helpers.actions import Action


class Reward(Agent):
    def __init__(self):
        super().__init__()
        self.prev_reward_ = None
        self.state_vector_ = list()
        self.exp_reward_ = dict()
        self.exp_frequency_ = dict()

    def getAction(self):
        v_state_k = str(self.state_vector_)
        e_reward = self.exp_reward_
        e_freq = self.exp_frequency_

        if v_state_k not in e_reward.keys():
            e_freq[v_state_k] = {act: 0 for act in Action}
            e_reward[v_state_k] = {act: 0 for act in Action}
        return super().getAction()

    def nextState(self, s, reward):
        v_state_k = str(self.state_vector_)
        e_reward = self.exp_reward_
        e_freq = self.exp_frequency_
        p_act = self.prev_action_

        # update the last action's expect reward increase it's frequency
        if self.init:
            count = e_freq[v_state_k][p_act]
            last = e_reward[v_state_k][p_act]
            self.update(count, last, reward)
        else: # first action
            self.init = True
            self.prev_action_ = -1

        self.prev_reward_ = reward
        self.state_ = s
        self.state_vector_ = s  # Original state
        # self.state_vector_ = s[2:] # Reduced state [B,S,F]
        # self.state_vector_ = [self.prev_action_] + s[2:] #A_previous + Reduced state [B,S,F]

    def update(self, count, last, reward):
        v_state_k = str(self.state_vector_)
        e_reward = self.exp_reward_
        e_freq = self.exp_frequency_
        p_act = self.prev_action_

        e_reward[v_state_k][p_act] = (last * count + reward) / (count + 1)
        e_freq[v_state_k][p_act] += 1
