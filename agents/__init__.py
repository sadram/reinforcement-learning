from os.path import dirname, basename, isfile
import glob

modules = glob.glob(dirname(__file__) + "/*.py")
obj = [basename(f)[:-3] for f in modules if isfile(f)]
obj.remove('__init__')
__all__ = ['softmax_policy',
           'ucb',
           'custom_policy',
           'agent',
           'reward',
           'qlearning_policy',
           'egreedy_policy',
           'greedy_policy']
# print(obj)
# __all__ = obj
del (obj, modules)
