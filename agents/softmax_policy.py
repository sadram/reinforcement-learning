import numpy as np

from agents.reward import Reward
from helpers.actions import Action


class SoftmaxPolicy(Reward):
    def __init__(self, tau=1.5):
        super().__init__()
        self.tau_ = tau

    def getActionPolicy(self):
        """Gibbs sampling from 8 possible actions"""
        avg_reward = [np.exp(e / self.tau_) for e in self.exp_reward_[str(self.state_vector_)].values()]
        total_reward = sum(avg_reward)
        softmax_proba = [p / total_reward for p in avg_reward]
        return Action(np.argmax(np.random.multinomial(1, softmax_proba)) + 1)