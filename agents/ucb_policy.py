import math

from agents.reward import Reward
from helpers.actions import Action


class UpperConfidenceBoundPolicy(Reward):
    def __init__(self, rho=0.0001):
        super().__init__()
        self.rho_ = rho
        self.UCB_ = dict()
        self.times_ = dict()

    def reset(self):
        super().reset()

    def getActionPolicy(self):
        v_state_key = str(self.state_vector_)

        if v_state_key not in self.UCB_.keys():
            self.UCB_[v_state_key] = {act: 0 for act in Action}
            self.times_[v_state_key] = 1
        return max(self.UCB_[v_state_key], key=self.UCB_[v_state_key].get)

    def update(self, freq, last, reward):
        super().update(freq, last, reward)
        v_state_key = str(self.state_vector_)
        times = self.times_

        times[v_state_key] += 1
        self.UCB_[v_state_key][self.prev_action_] = \
            (last * freq + reward) / (freq + 1) + \
            self.rho_ * math.sqrt(2 * math.log(times[v_state_key]) / (freq + 1))
