import numpy as np

from agents.agent import Agent
from helpers.actions import Action as A, ACTION_SIZE


class CustomPolicy(Agent):
    def __init__(self):
        super().__init__()
        self.danger_ = False

    def getActionPolicy(self):
        """We avoid pitfalls and try to flash the wumpus"""
        c_st = self.state_
        # if wumpus smelled try to flash it
        if c_st[2] and c_st[4] > 0:
            self.danger_ = False
            return self.flash()
        # if we sense a danger_ move to a safe place
        elif (c_st[2] or c_st[3]) and (not self.danger_):
            self.danger_ = True
            return self.safeMove()
        else:
            self.danger_ = False
            return self.getMoveRandom()

    def safeMove(self):
        """move to a place we flashed or were we came from"""
        p_act = self.prev_action_
        if (p_act == A.DOWN) or (p_act == A.FLASH_UP):
            return A.UP
        elif (p_act == A.UP) or (p_act == A.FLASH_DOWN):
            return A.DOWN
        elif (p_act == A.RIGHT) or (p_act == A.FLASH_LEFT):
            return A.LEFT
        elif (p_act == A.LEFT) or (p_act == A.FLASH_RIGHT):
            return A.RIGHT
        # in case we add other actions like diagonal moves in the future
        else:
            raise TypeError("No such action {}".format(p_act))

    def flash(self):
        """flash or moved were we flashed to avoid infinite flashing"""
        p_act = self.prev_action_
        # flash to move
        if p_act == A.UP:
            return A.FLASH_UP
        elif p_act == A.DOWN:
            return A.FLASH_DOWN
        elif p_act == A.LEFT:
            return A.FLASH_LEFT
        elif p_act == A.RIGHT:
            return A.FLASH_RIGHT
        # move to flash
        elif p_act == A.FLASH_UP:
            return A.UP
        elif p_act == A.FLASH_DOWN:
            return A.DOWN
        elif p_act == A.FLASH_LEFT:
            return A.LEFT
        elif p_act == A.FLASH_RIGHT:
            return A.RIGHT
        # in case we add other actions like diagonal moves in the future
        else:
            raise TypeError("No such action {}".format(p_act))

