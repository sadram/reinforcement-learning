from agents.reward import Reward


class GreedyPolicy(Reward):
    def __init__(self):
        super().__init__()

    def getActionPolicy(self):
        v_state_key = str(self.state_vector_)
        e_reward = self.exp_reward_
        return max(e_reward[v_state_key], key=e_reward[v_state_key].get)
