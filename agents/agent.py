import numpy as np

from helpers.actions import Action, ACTION_SIZE
from datetime import datetime


class Agent:
    def __init__(self):
        self.init = None
        self.prev_action_ = None
        self.state_ = None
        np.random.seed(int(datetime.now().strftime("%f")))
        self.reset()

    def reset(self):
        self.init = False
        self.prev_action_ = None
        self.state_ = None

    def getAction(self):
        self.prev_action_ = self.getActionPolicy()
        return self.prev_action_

    def getActionPolicy(self):
        return self.getActionRandom()

    @staticmethod
    def getActionRandom():
        return Action(np.random.randint(1, ACTION_SIZE + 1))

    @staticmethod
    def getMoveRandom():
        return Action(np.random.randint(1, (ACTION_SIZE / 2) + 1))

    def getPosition(self):
        return self.state_[:2]

    def getState(self):
        return self.state_

    def nextState(self, s, reward):
        self.state_ = s
