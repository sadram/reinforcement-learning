import numpy as np

from agents.reward import Reward
from helpers.actions import Action


class QLearningPolicy(Reward):
    def __init__(self, epsilon=0.1, tau=1.0, gamma=1.0, alpha=0.1, softmax=0):
        super().__init__()
        self.Q_ = dict()
        self.alpha_ = alpha
        self.epsilon_ = epsilon
        self.gamma_ = gamma
        self.tau_ = tau
        self.softmax = softmax

    def getAction(self):
        return super().getAction()

    def getNextAction(self):
        # Softmax policy
        if self.softmax == 1:
            if self.Q_:
                softmax_proba = [np.exp(e) / self.tau_ for e in self.Q_[str(self.state_vector_)].values()]
                total_reward = sum(softmax_proba)
                softmax_proba = [p / total_reward for p in softmax_proba]
                return Action(np.argmax(np.random.multinomial(1, softmax_proba)) + 1)
            else:
                return self.getActionRandom()
        # e-greedy
        if self.softmax == 0:
            draw = np.random.uniform()
            if draw > self.epsilon_ and self.Q_:
                return max(self.Q_[str(self.state_vector_)], key=self.Q_[str(self.state_vector_)].get)
            else:
                return self.getActionRandom()

    def nextState(self, s, reward):
        if self.init:
            oldstate = str(self.state_vector_)
            if oldstate not in self.Q_.keys():
                self.Q_[oldstate] = {act: 0 for act in Action}
            qold = self.Q_[oldstate][self.prev_action_]

        super().nextState(s, reward)
        if self.prev_action_ == -1:
            return

        n_state = str(self.state_vector_)
        if n_state not in self.Q_.keys():
            self.Q_[n_state] = {act: 0 for act in Action}

        n_state = str(self.state_vector_)
        qmax = max(self.Q_[n_state], key=self.Q_[n_state].get)
        deltav = reward + self.gamma_ * self.Q_[n_state][qmax] - qold

        self.Q_[oldstate][self.prev_action_] += self.alpha_ * deltav
