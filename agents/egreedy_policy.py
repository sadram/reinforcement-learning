import numpy as np

from agents.greedy_policy import GreedyPolicy


class EpslionGreedyPolicy(GreedyPolicy):
    def __init__(self, epsilon=0.25):
        super().__init__()
        self.epsilon_ = epsilon

    def getActionPolicy(self):
        """A Greedy policy with 25% of random exploration"""
        if np.random.binomial(1, 1 - self.epsilon_):
            return super().getActionPolicy()
        else:
            return super().getActionRandom()
